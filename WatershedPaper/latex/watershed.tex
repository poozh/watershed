%\documentclass[10pt, conference, compsocconf]{IEEEtran}
\documentclass{article}
\usepackage{times}
\usepackage{xcolor}
\usepackage{mathtools}
\usepackage{enumerate}
\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{subfig}
\usepackage{amsmath}
\usepackage{eqnarray}
\usepackage[]{algorithm}
\usepackage{clrscode3e}
\usepackage[pdftex]{graphicx}
\usepackage{amsthm}

\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}

\newtheorem{lemma}{Lemma}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% TITLE
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Watershed segmentation of edge-weighted graphs: algorithm and
  theoretical properties}

\author{
Aleksandar Zlateski\\
EECS Dept.\\
Massachusetts Institute of Technology\\ Cambridge, MA\\
{\tt\small zlateski@mit.edu}
\and
H. Sebastian Seung\\
Neuroscience Institute and\\
Computer Science Dept.\\
Princeton University\\ Princeton, NJ 08544\\
{\tt\small sseung@princeton.edu}
}



\maketitle
%\thispagestyle{empty}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% ABSTRACT
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{abstract}
  We discuss watershed segmentation of an edge-weighted undirected
  graph.  We present an algorithm that computes a directed graph that
  defines a unique direction of steepest ascent for every vertex, and
  then finds the basins of attraction of this steepest ascent
  dynamics.  The algorithm is similar to ``watershed cuts,'' except
  that it divides non-maximal plateaus more evenly
  \cite{Cousty2009,Cousty2010}.
% Every steepest ascent path is guaranteed to converge to a regional
% maximum of the affinity graph, and escape from any plateau through
% the closest plateau corner.
  The resulting watershed basins constitute a flat segmentation of the
  affinity graph. Applying single linkage clustering to the watershed
  basins yields a hierarchy of segmentations.  We prove that this is a
  ``natural'' definition of hierarchy, in the sense that each level is
  equivalent to the flat watershed segmentation of some upper
  thresholding of the affinity graph. The time complexity scales
  quasilinearly with the number of edges, making our algorithm
  practical even for very large graphs.

  Smoothing the affinity graph by upper thresholding is equivalent to
  merging basins a threshold.

\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% INTRODUCTION
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}
Many interesting computational problems can be formulated as the
partitioning or segmentation of the vertices of an edge-weighted
undirected graph.  The weight of an edge between two vertices
represents their ``affinity'' for each other, so we will use the term
``affinity graph.''  A common approach to segmentation is to formulate
an objective function favoring placement of high affinity vertex pairs
in the same segment, and low affinity pairs in different segments.
Then an optimization algorithm is applied to obtain a segmentation
\cite{shi2000normalized,kolmogorov2002energy}.  These algorithms
typically run in polynomial time, but the exponent is larger than one,
limiting their applicability to very large graphs.
% say smth about vertex-weighting

On the other hand, the family of watershed algorithms achieves linear
time complexity, and is therefore practical for large graphs.  Roughly
speaking, these algorithms find the basins of attraction of a steepest
ascent dynamics on the graph.  The basins are in one-to-one
correspondence with the regional maxima of the graph.  The definition
becomes subtle in regions of the graph that are ``flat'' but not
regional maxima.  In such ``non-maximal plateaus,'' the direction of
steepest ascent is not uniquely defined.

Here we propose a new definition for watershed segmentation that is
similar to the previously proposed ``watershed cuts''
\cite{Cousty2009,Cousty2010}, except that non-maximal plateaus are
divided more evenly.  The steepest ascent path from any plateau vertex
is defined as the unique path that exits the plateau in the shortest
possible time.  This idea was previously proposed for watershed
segmentation of images [Aleks: provide citation here], but is
apparently novel for edge-weighted graphs.  We also provide an
algorithm that computes the watershed segmentation in linear time.

Watershed algorithms generally give rise to severe oversegmentation,
because noise creates many regional maxima, and every regional maximum
corresponds to a watershed basin.  If prior knowledge enables the
placement of one ``marker'' in each object, then marker-based
watershed algorithms can be applied to divide the graph into one
region per marker [cite].  However, no such prior knowledge exists in
most interesting cases.  Then two methods are commonly used to reduce
oversegmentation, smoothing and agglomeration.  Smoothing the graph
reduces noise and hence reduces the number of regional maxima, after
which the watershed algorithm can be applied.  Agglomeration of
watershed basins yields a fine-to-coarse hierarchy of segmentations
with progressively fewer and larger regions.

Here we focus attention on agglomeration by single linkage clustering.
This method is not expected to produce performance superior to more
sophisticated agglomeration methods \cite{beucher1994watershed}.
However, we feel that this method is ``natural'' because it is
equivalent to another method of reducing oversegmentation based on
smoothing.  Namely, single linkage clustering is equivalent to
smoothing the affinity graph via upper thresholding, i.e., setting all
affinities above a threshold $\theta$ to infinity.  We will prove that
varying $\theta$ yields exactly the same hierarchy of segmentations as
single linkage clustering of the watershed basins of the original
affinity graph.

For the sake of concreteness, the text and figures will often consider
the case of an affinity graph in which the vertices are image pixels
or voxels, and the edges are between nearest neighbor voxels.
However, the definitions and algorithms are actually applicable to
arbitrary affinity graphs.

Finally, we provide a {\tt C++} implementation for the special case of
affinity graphs in which vertices represent the voxels of 3D images,
and edges represent nearest neighbor pairs of voxels.  The results of
the algorithm are illustrated by segmenting images from 3D electron
microscopy.



\section{Flat watershed segmentation}
Consider a connected graph $G=(V,E)$ with non-negative edge
weights. An edge $\{u,v\}$ is \emph{locally maximal} for $u$ if there
is no other edge incident to $u$ with higher weight.  A \emph{steepest
  ascent path} $\left\langle v_{0},e_{0},v_{1},e_{1},v_{2}, \dots
\right\rangle$ is one for which every edge $e_{i}$ is locally maximal
with respect to $v_{i}.$

A \emph{regional maximum} $M$ is a connected subgraph of $G$ such that
there is a steepest ascent path between any pair of vertices in $M$,
and every steepest ascent path starting in $M$ will stay within $M$. A
vertex $v$ belongs to the \emph{basin of attraction} of a regional
maximum $M$ if there exists a steepest ascent path from $v$ to some
vertex in $M$.

In the special case where all edge weights take on unique values, the
locally maximal edge is uniquely defined for each vertex, and hence
the steepest ascent path is also uniquely defined.  It follows that
every vertex belongs to exactly one basin of
attraction.\footnote{Every basin consists of two vertices, and the
  steepest ascent path within a basin cycles between the two
  vertices.}  The basins constitute a segmentation of the graph.

[describe standard algorithms for connected components.  best
  algorithm depends on representation of graph BFS/DFS.  faster for
  list of vertices disjoint sets.  faster for list of edges]

More generally, a locally maximal edge for a vertex may not be unique,
because of the possibility of ties.  It follows that a steepest ascent
path starting from a vertex may not be unique, and that a vertex may
belong to more than one basin of attraction.  Furthermore, there may
exist steepest ascent paths that never converge to a regional maximum
but remain stuck on non-maximal ``plateaus.''  In this case, the
definition of the watershed transform is more subtle.

The ``watershed cuts'' algorithm \cite{Cousty2009,Cousty2010}
alternates between depth first search (DFS) and breadth first search
(BFS) for a regional maximum.  It starts with DFS, switches to BFS
when it enters a plateau, switches back to DFS when it exits a
plateau, and continues alternating in this way until a regional
maximum is found.  This solution to the plateau problem is reasonable,
but has no guarantee of dividing plateaus evenly.

Our algorithm splits the search process into several passes through
the affinity graph.  The first pass explicitly computes a steepest
ascent graph. The second pass eliminates more edges from the steepest
ascent graph so that all plateau paths exit in the shortest possible
time (our definition of ``even'' plateau division). The third pass
finds the watershed basins by computing the connected components of
the steepest ascent graph.

\begin{figure}
  \centering
  \subfloat[]{\protect\includegraphics[scale=0.66]{fig/affinity_graph}}
  \subfloat[]{\protect\includegraphics[scale=0.66]{fig/sd_graph}}\\
  \subfloat[]{\protect\includegraphics[scale=0.66]{fig/sd_graph_plateaus}}
  \subfloat[]{\protect\includegraphics[scale=0.66]{fig/ws_result}}

  \protect\caption{The plateau division problem. (a) Affinity graph;
    (b) Retaining only maximal edges for each vertex yields this
    steepest ascent graph; (c) locally maximal plateaus (black),
    non-maximal plateau (dark gray), saddle vertex (S), plateau
    corners (C); (d) the two basins of attractions and border vertices
    (dark gray)}
\end{figure}

\subsection{Steepest ascent graph, version 1}
The starting point is an edge-weighted undirected graph $G$.  Replace
each edge of $G$ by edges in both directions between the same vertices
(Fig. 1a).  Then for every vertex retain outgoing edges with maximal
weight and eliminate other outgoing edges.  The resulting graph,
called $S_1$ is directed and unweighted.

By construction, any (directed) path in $S_1$ is a path of steepest
ascent in $G$. If a pair of vertices has edges in both directions,
they are represented by a single ``bidirectional edge'' in Fig. 1b.
Therefore, the edges of any vertex in $S_1$ can be classified as
incoming, outgoing, and bidirectional.

% note that incoming/outgoing now means smth else than it first did

% A steepest descent graph can be defined analogously using edges of
% minimal weight. Either steepest ascent or descent can be used
% without loss of generality.

\begin{figure}
  \centering
  \subfloat[]{\protect\includegraphics[scale=0.66]{fig/sd_graph_ordered}}
  \subfloat[]{\protect\includegraphics[scale=0.66]{fig/sd_graph_plateaus_ordered}}\\
  \subfloat[]{\protect\includegraphics[scale=0.66]{fig/sd_graph_plateaus_modified}}
  \subfloat[]{\protect\includegraphics[scale=0.66]{fig/final_segmentation}}

  \protect\caption{Watershed algorithm that divides plateaus
    evenly. (a) existence of a vertex ordering is assumed; (b) Every
    plateau vertex is labeled by its graph distance to the nearest
    plateau corner; (c) the steepest ascent graph defines unique paths;
    (d) final watershed segmentation}
\end{figure}

\subsection{Steepest ascent graph, versions 2 and 3}
If a vertex of $S_1$ has
\begin{enumerate}
\item more than one outgoing edge,
\item both outgoing and bidirectional edges,
\item or more than one bidirectional edge,
\end{enumerate}
then more than one path exists from that vertex.  We will modify $S_1$
in a way that eliminates all vertices of categories (1) and (2) and
some vertices of category (3).  Note that the three categories may
overlap. Also ``elimination'' of a vertex from a category does not
mean removal of the vertex from the graph, but modifying its edges so
that it no longer falls into that category.

\subsubsection{Elimination of saddle vertices and identification of plateau exits}

In addition to the affinity graph $G$, the algorithm is assumed to
receive another input, an ordering of the vertices of $G$ that is used
for tie-breaking.

For all vertices, we perform the following.  If we encounter a vertex
with multiple outgoing edges (category 1 above), we identify the
``winning'' edge as the one pointing to the vertex that comes first in
the ordering.  We retain the ``winning'' edge and remove the others.
If a vertex has both outgoing and bidirectional edges, it is
identified as category 2.  This procedure yields $S_2$, the second
version of the steepest ascent graph, and a list of category 2
vertices.

% move later?
%\footnote{Note that the elimination of saddle vertices is not integrated in the
%creation of $S_1$.  A separate pass is required because knowing the
%number of outgoing edges depends on identifying other edges as
%bidirectional.}

\subsubsection{Division of non-maximal plateaus}

Using the category 2 vertices as starting points, we perform breadth
first search (BFS) for bidirectional edges.  For each vertex reached
by the search, we convert all bidirectional edges to incoming edges.
At each depth of the search, we use the same ordering of the vertices
as before. The output of this procedure is $S_3$, the third version of
the steepest ascent graph.

The difference between $S_2$ and $S_3$ can be described as follows.  A
\emph{plateau} is defined as a connected component of the subgraph of
$S_2$ containing only bidirectional edges.  A category 2 vertex is a
plateau vertex, given that it has bidirectional edges.  It also has an
outgoing edge, through which a path can leave the plateau.  Therefore
we refer to a category 2 vertex in $S_2$ as a \emph{plateau exit}.
% there can't be more than one, since saddles were eliminated, right?
% if we want this to
Plateaus can be classified based on the presence or absence of exits.

A \emph{non-maximal} plateau contains one or more exits.  This type of
plateau is explored by the BFS procedure, which starts from plateau
exits.  If a directed path in $S_2$ enters a non-maximal plateau, it
may either (1) remain within the plateau for all time, or (2) leave
through an exit.  The BFS procedure eliminates the first possibility,
so that all directed paths are guaranteed to leave the plateau.
Furthemore, a directed path is guaranteed to reach an exit in the
shortest possible time.  In other words, from each vertex in the
plateau, a unique directed path leads to the nearest exit, where
nearest is defined based on graph distance.  If there is a tie between
exits, it is resolved using the same vertex ordering as before.

\emph{Locally maximal} plateaus contain no exits; they are
equivalent to regional maxima of the original graph.
% why are we saying ``locally'' instead of ``regionally''?
% alternative term is invariant plateau
This type of plateau is not explored by the BFS procedure. Therefore
$S_3$ is no different from $S_2$ in locally maximal plateaus.  On the
other hand, $S_3$ differs from $S_2$ in non-maximal plateaus, where
all bidirectional edges are converted to unidirectional edges.

The BFS is implemented as follows.  We initialize a global FIFO queue
$Q$, mark all the corner vertices as visited and insert them into $Q$
in increasing order of their index. While $Q$ is not empty we remove
the vertex $v$ from the front of the queue, and loop over all its
bidirectional edges $\{v,u\}$.  If $u$ has not yet been visited, we
mark it as visited, insert it to the back of the queue, and change the
edge from bidirectional to incoming $(v\leftarrow u)$. If $u$ has
already been visited, we remove the bidirectional edge $\{v,u\}$
completely.

\subsection{Connected components}
To summarize the dynamics of the third and final steepest ascent graph
$S_3$, all paths eventually converge to locally maximal plateaus.
Every path is unique until it reaches a locally maximal plateau.  Once
a path enters a locally maximal plateau, it remains within the plateau
for all future time and may become nonunique.  On the way to a locally
maximal plateau, if a path enters a non-maximal plateau, it will exit
in the shortest possible time (Fig. 2c).

Since the path from any vertex to its corresponding regional maximum
is unique, $S_3$ can be segmented into the basins of attraction of the
dynamics.  This is done by regarding $S_3$ as an undirected graph and
computing its connected components, for example by depth first search.
The resulting connected components are called \emph{watershed basins},
and are the output of watershed segmentation.  The non-maximal
plateaus will be evenly divided, in the sense that every plateau
vertex will belong to the same watershed basin as its nearest plateau
exit.

\subsection{Time complexity}
As we operate on a connected graph we assume $O(|E|) \ge O(|V|)$.  To
create $S_1$, we visit each edge a constant number of times. To create
$S_2$, we visit each vertex at most once, and edges incident to it a
constant number of times. The complexity of connected components is
$O(|E|)$.  Therefore the overall complexity is $O(|E|)$.

\subsection{Properties of watershed segmentation}
Basins are in one-to-one correspondence with regional maxima.

% maximin affinity between two regional maxima

The algorithm produces an optimal partitioning as defined
in~\cite{Cousty2009}.

explanation:

spanning tree of final steepest ascent graph in a single basin is
maximal spanning tree of corresponding subgraph of affinity graph

maximal spanning forest with exactly n trees

\section{Reducing oversegmentation by merging basins}
The watershed segmentation is typically fragmented into an overly
large number of small basins.  This is because the basins are in
one-to-one correspondence with local maxima of the graph, and noise in
the affinities can give rise to a large number of local maxima.  To
reduce oversegmentation, one approach is to define a new affinity
graph in which vertices represent basins, and apply some segmentation
algorithm to this ``basin affinity graph.''  Typically, an edge exists
in the basin affinity graph if and only if there exists an edge
between the two basins in the original affinity graph.  The basin
affinities can be defined in many ways, and this definition is left
unspecified for the time being.

A particularly simple algorithm for segmenting a graph is to find its
connected components after lower thresholding at $\theta$.
\begin{definition}
Lower thresholding a graph at $\theta$. Removing all edges with
weights less than $\theta$.
% less than or equal?
\end{definition}
Applying this algorithm for multiple values of $\theta$ yields a
family of segmentations, where larger $\theta$ means a finer
segmentation.  The family is hierarchical, meaning that if
$\theta_2>\theta_1$, then segmentation $S_{\theta_2}$ is a refinement
of $S_{\theta_1}$, i.e., every segment in $S_{\theta_1}$ is equal to
the union of one or more segments of $S_{\theta_2}$.

If we would like to be able to quickly compute the connected
components for any value of $\theta$, we can precompute the maximal
spanning tree (MST) of the basin affinity graph (or maximal spanning
forest if the graph is not connected).  The connected components of
the MST after lower thresholding are the same as those of the basin
affinity graph after lower thresholding.

The MST can be computed by sorting the edges of the graph in order of
decreasing affinity. Then apply Kruskal's algorithm to find the MST.
The time complexity is dominated by the sort, and is $O(E_{basin}\log
E_{basin})$, where $E_{basin}$ is the number of edges in the basin
affinity graph.

% Prim's algorithm is $O(E\log V)$ so might be faster but more memory fragmentation.

Kruskal's algorithm is commonly implemented using a disjoint-sets data
structure.
% linked list, forest?  root IDs?
The sequence of merge operations can be represented by a tree called a
\emph{dendrogram}.  If we view the dendrogram as the output (rather
than the MST), Kruskal's algorithm is equivalent to single linkage
clustering (SLC) starting from the watershed basins.  The leaves of
the dendrogram represent the basins.  Each internal vertex is the root
of a subtree, and represents an ``agglomerated cluster,'' one
generated by all the merging all the basins in the subtree.  The
height of the internal vertex represents the affinity at which the
last merge operation in the subtree occurs.

The MST contains similar but different information than the
dendrogram.  Internal vertices of the dendrogram represent
agglomerated clusters, while vertices of the MST are leaf clusters.
Each edge of the MST corresponds to the merge operation that makes its
leaf clusters belong to the same agglomerated cluster.
% mention root IDs of disjoint sets?

\begin{figure}
\caption{basin affinity graph, basin MST, basin dendrogram}
\end{figure}

The equivalence of all the above methods of segmenting a graph is not
specific to watershed basins, and is summarized by the following
Lemma generally applicable to any affinity graph.
\begin{lemma}
The connected components of an
\begin{enumerate}
\item affinity graph after lower thresholding at $\theta$,
\item its MST after lower thresholding at  $\theta$,
\item and its SLC dendrogram after removing vertices with height
  greater than or equal to $\theta$
\end{enumerate}
are equivalent.
\end{lemma}
Proofs are omitted as they are simple and the equivalence is well-known.

\section{Reducing oversegmentation by smoothing}
The previous section concerned the basin affinity graph, which was
constructed from the watershed basins derived from the original
affinity graph.  To avoid confusion, we will henceforth refer to the
original affinity graph as the ``voxel affinity graph.''  Although
this term comes from the specific application to 3D image
segmentation, the mathematics is generally applicable to any kind of
affinity graph.  We will refer to the edge weights in the basin graph
as ``basin affinities'' if there is risk of confusion with the
original ``voxel affinities.''  For short we will say ``affinities''
if it is clear from the context whether we are discussing basins or
voxels.

In the previous section, the definition of basin affinities was left
unspecified.  We now specialize to a particular definition.
\begin{theorem}
  Define the affinity of two basins to be the maximum affinity of
  edges between the two basins in the voxel affinity graph.  Then the
  following are equivalent:
  \begin{enumerate}
\item Connected components of the basin affinity graph after lower
  thresholding at $\theta$.
\item Watershed basins of the voxel affinity graph after upper
  thresholding at $\theta$.
  \end{enumerate}
\end{theorem}
The Theorem makes use of another thresholding operation for a graph.
\begin{definition}
Upper thresholding a graph at $\theta$. Replacing all
  weights larger than $\theta$ by $\theta$.
\end{definition}
When applied to an affinity graph, upper thresholding can be viewed as
a kind of ``smoothing'' operation.  It will tend to reduce the number
of local maxima by creating maximal plateaus, and hence reduce
oversegmentation.  The implication of the Theorem is that smoothing
before watershed can be equivalent to merging basins after watershed,
as a method of reducing oversegmentation.

Upper thresholding reduces computational cost, measured in both time
and memory.
% can we make this more precise
This comes at the expense of losing the ability to compute the finer
segmentations in the hierarchy.

\begin{proof}
  Let $G(\theta)$ be the result of upper thresholding the voxel
  affinity graph $G$ at $\theta$. Let $W$ and $W(\theta)$ be the
  watershed segmentations of $G$ and $G(\theta)$, respectively.  We
  would like to show that $W(\theta)$ is equivalent to the connected
  components of the $W$-basin affinity graph after lower thresholding
  at $\theta$.

  Let $S_2$ and $S_2(\theta)$ be the (version 2) steepest ascent
  graphs obtained from $G$ and $G(\theta)$, respectively.  Vertices of
  $S_2$ not incident to any edge with affinity smaller than $\theta$
  will stay unmodified, even after plateau division. All other
  vertices of $S_2$ will become part of a locally maximal plateau of
  $S_2(\theta)$.  All locally maximal edges of $S_2$ will stay locally
  maximal. Therefore the only effect of the affinity threshold is to
  introduce bidirectional edges. It follows that a connected component
  of $S_2$ must be a subset of a connected component of $S_2(\theta)$.
  In other words, every $W$-basin is a subset of some
  $W_\theta$-basin.  Since the $W$-basins are a segmentation of the
  graph, it follows that every $W_\theta$-basin is a union of
  $W$-basins.  It only remains to be shown that this union is a
  connected component of the $W$-basin affinity graph after lower
  thresholding at $\theta$.

This is the case, because the following statements are equivalent:
\begin{enumerate}
\item Two $W$-basins are contained in the same $W_\theta$-basin.
\item The two regional maxima in $G$ corresponding to the $W$-basins
  are contained in the regional maximum of $G_\theta$ corresponding to the
  $W_\theta$-basin.
\item There is a path between the two regional maxima in $G$ with
  all affinities greater than $\theta$.
\item There is a path between the two $W$-basins in the
  basin graph after lower thresholding at $\theta$.
\end{enumerate}
\end{proof}


\section{Connected components of the voxel affinity graph}
As discussed above, a simple algorithm for segmenting the basin
affinity graph is to find the connected components after lower
thresholding.  This simple algorithm could also be applied directly to
the voxel affinity graph, rather than the watershed algorithm.  The
relationship between these two segmentations is

\begin{theorem}
  Define the affinity of two basins to be the maximum affinity of
  edges between the two basins in the voxel affinity graph.  Then any
  connected component of the voxel affinity graph after lower
  thresholding at $\theta$ is contained in a connected component of
  the basin affinity graph after lower thresholding at $\theta$.
\end{theorem}

Many connected components of the voxel affinity graph will resemble
``shrunken'' versions of connected components of the basin affinity
graph.  They are shrunken because

\begin{proof}
  Suppose that two voxels $v_1$ and $v_2$ belong to the same connected
  component of the voxel affinity graph after lower thresholding at
  $\theta$, and suppose that $v_1$ and $v_2$ belong to basins $b_1$
  and $b_2$.  There exists a path between $v_1$ and $v_2$ in the voxel
  affinity graph with all affinities greater than $\theta$.  Some of
  the edges of this path cross between basins.  For each pair of
  basins, it follows that the affinity is greater than $\theta$.
  Therefore there exists a path in the basin affinity graph from $b_1$
  to $b_2$ with all affinities greater than $\theta$.  It follows that
  $b_1$ and $b_2$ (and $v_1$ and $v_2$) must belong to the same
  connected component of the basin affinity graph.
\end{proof}

\section{Vertex-weighted graphs}

% Not sure about this!

% The watershed algorithm has been formulated for edge-weighted graphs.
% The extension to vertex-weighted graphs is straightforward.  Given a
% vertex-weighted graph, define an edge-weighted graph with the same
% vertices and edges.  Each edge weight is the maximum of the two vertex
% weights.  Then a steepest ascent path in the edge-weighted graph is
% also a steepest ascent path in the vertex-weighted graph, and the
% watershed basins of the two graphs are the same.

% The case of vertex-weighted graphs is relevant for images.  Since the
% vertices of the corresponding affinity graph represent image voxels,
% in many contexts it is natural to weight the vertices (rather than the
% edges) using the voxel values.

The watershed algorithm has been formulated for edge-weighted graphs.
The algorithm can be extended to vertex-weighted graphs in the
following way:

First, the graph is extended by introducing $|V|$ new vertices.  For
each vertex $v$ in $V$ a new vertex $v'$ is introduced with the same
weight.  Further, the set of edges is extended by introducing an edge
between each pair of $v$ and $v'$.

An edge-weighted graph is then created on the same set of vertices and
edges (of the extended graph) with the weight of the edge equal to the
minimal weight of the two incident vertices.

In the final segmentation the introduced edges and vertices are then
ignored.

Note that the vertex-weighted graph can have a single vertex as a
local maximum, the introduction of the new vertices allows us to use
the edge-weighted algorithm.  The steepest ascent path in the
edge-weighted graph will also be a steepest ascent in the
vertex-weighted graph, and the watershed basins of the two graphs are
the same.

The case of vertex-weighted graphs is relevant for images.  Since the
vertices of the corresponding affinity graph represent image voxels,
in many contexts it is natural to weight the vertices (rather than the
edges) using the voxel values.


\section{Source code}
3D image
Nearest neighbor affinity graph

\section{Discussion}
To show confidence about high values of \emph{disaffinities}, and in
order to prevent undesired mergers, we introduce a threshold
$\theta_{\min}$ by eliminating all edges with affinity less than
$\theta_{\min}$. The lower threshold can produce singleton vertices in
$G$. The singleton vertices are not assigned to any watershed basin
and are considered background, which is often a desired result.

mention possible improvements to hierarchy

Like watershed cuts, our algorithm has linear time complexity.  The
use of multiple passes makes it possible to divide plateaus evenly.  A
second motivation for our algorithm is that it separates the
computation into two passes that are parallelizable versus two passes
that are not.  A parallel variant of our algorithm will be described
in a future paper.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% REFERENCES
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


{\small
\bibliographystyle{ieeetr}
\bibliography{./ref/bib}
}

\end{document}

\subsection{Assigning border vertices}
In Fig. 1(d) we show the \emph{basins of attraction} of the two
\emph{regional minima}. The \emph{border} vertices are shown in dark
gray and belong to both \emph{basins of attraction}. Watershed
cuts~\cite{Cousty2009,Cousty2010} assign \emph{border} vertices with a
single constraint that all the \emph{basins of attraction} have to be
connected.

one can merge watershed basins to create a
smaller number of larger regions.  Furthermore, merging can be used to
generate a hierarchy of segmentations that vary from fine to coarse
\cite{beucher1994watershed, Najman1996}.

The basin affinities are found by looping over all edges in the voxel
affinity graph.  For every pair of basins, we keep track of the
largest voxel affinity seen so far.  At the end, we have the basin
affinity graph represented as a list of edges.  The time complexity is
$O(E_{voxel})$, the number of edges in the voxel affinity graph.
